(defproject sample-ordered-aot "0.1.0-SNAPSHOT"
  :description "This project is to drive testing of ordered aot compilation."
  :url "https://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "https://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :aot :all)
